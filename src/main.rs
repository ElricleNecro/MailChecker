extern crate argparse;
extern crate maildir;

use argparse::{ArgumentParser, Store, List, Print};

mod command;
use self::command::Command;

pub fn main() {
    let mut cmd: Command = Command::Unread;
    let mut args: Vec<String> = Vec::new();

    {
        let mut ap = ArgumentParser::new();

        ap.set_description("A Mail User Agent in CLI.");

        ap.refer(&mut cmd)
            .required()
            .add_argument("Command", Store, "Command to run (unread, search).");
        ap.refer(&mut args)
            .add_argument("Arguments", List, "Argument for the command.");

        ap.add_option(&["-V", "--version"], Print("0.1a".to_string()), "Show version");

        ap.stop_on_first_argument(true);

        ap.parse_args_or_exit();
    }

    args.insert(0, format!("subcommand {}", cmd));
    cmd.process(args);
}
