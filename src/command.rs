use argparse::{ArgumentParser, Store, StoreTrue};

use maildir as md;

use std::str::FromStr;
use std::path::Path;
use std::io::{stdout, stderr};
use std::fmt::{Display, Formatter, self};
use std::process as stdp;


pub enum Command {
    Unread,
    Search,
    List,
}

impl FromStr for Command {
    type Err = String;

    fn from_str(src: &str) -> Result<Command, String> {
        match src {
            "unread" => Ok(Command::Unread),
            "search" => Ok(Command::Search),
            "list" => Ok(Command::List),
            _ => Err("String should be one of (unread, search, list)".to_string()),
        }
    }
}

impl Display for Command {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            &Command::Unread => write!(f, "unread"),
            &Command::Search => write!(f, "search"),
            &Command::List => write!(f, "list"),
        }
    }
}

impl Command {
    pub fn process(&self, args: Vec<String>) {
        match *self {
            Command::Unread => self.unread(args),
            Command::Search => self.search(args),
            Command::List => self.list(args),
        }
    }

    fn list(&self, args:Vec<String>) {
        let mut md_dir = "/home/plum/.email/gmx".to_string();
        let mut mbox = "all".to_string();

        {
            let mut ap = ArgumentParser::new();

            ap.set_description("List unread mails.");

            ap.refer(&mut md_dir)
                .add_option(&["-p", "--path"], Store, "Maildir to deal with.");
            ap.refer(&mut mbox)
                .add_option(&["-b", "--boxes"], Store, "Boxes to check (default: all)");

            match ap.parse(args, &mut stdout(), &mut stderr()) {
                Ok(_) => (),
                Err(x) => stdp::exit(x),
            }
        }

        let my_box = md::Boxes::new(Path::new(&md_dir));

        for (v, _) in my_box.iter() {
            println!("{}", v);
        }
    }

    fn unread(&self, args:Vec<String>) {
        let mut md_dir = "/home/plum/.email/gmx".to_string();
        let mut mbox = "all".to_string();
        let mut list = false;

        {
            let mut ap = ArgumentParser::new();

            ap.set_description("List unread mails.");

            ap.refer(&mut md_dir)
                .add_option(&["-p", "--path"], Store, "Maildir to deal with.");
            ap.refer(&mut mbox)
                .add_option(&["-b", "--boxes"], Store, "Boxes to check (default: all)");
            ap.refer(&mut list)
                .add_option(&["-l", "--list"], StoreTrue, "List new emails.");

            match ap.parse(args, &mut stdout(), &mut stderr()) {
                Ok(_) => (),
                Err(x) => stdp::exit(x),
            }
        }

        let my_box = md::Boxes::new(Path::new(&md_dir));
        let mut mails = match mbox.as_str() {
            "all" => {
                match my_box.get_new() {
                    Some(x) => x,
                    None => {
                        println!("No new mail.");
                        stdp::exit(-1);
                    }
                }
            },
            _ => {
                match my_box.get(&mbox) {
                    Some(m) => {
                        match m.get_new() {
                            Some(x) => x,
                            None => {
                                println!("No new mail in {}.", mbox);
                                stdp::exit(-1);
                            }
                        }
                    },
                    None => {
                        println!("No sub-boxes called {}.", mbox);
                        stdp::exit(-1);
                    }
                }
            },
        };

        if list {
            for mail in &mut mails {
                mail.parse();
                // println!("From: '{}', Subject: '{}'", );
            }
        }

        // match mbox.as_str() {
            // "all" => {
                // match my_box.get_new() {
                    // Some(x) => println!("Total new mails: {}", x.len()),
                    // None => println!("No new mail"),
                // }
            // },
            // _ => {
                // match my_box.get(&mbox) {
                    // Some(m) => {
                        // match m.get_new() {
                            // Some(x) => println!("Total new mails in {}: {}", mbox, x.len()),
                            // None => println!("No new mail"),
                        // }
                    // },
                    // None => {
                        // println!("No boxes called {}", mbox);
                        // std::process::exit(-1);
                    // }
                // }
            // },
        // }
    }

    fn search(&self, args: Vec<String>) {
        let mut md_dir = "/home/plum/.email/gmx".to_string();

        // Arguments parsing:
        {
            let mut ap = ArgumentParser::new();

            ap.set_description("Search across mails.");

            ap.refer(&mut md_dir)
                .add_option(&["-p", "--path"], Store, "Maildir to deal with.");

            match ap.parse(args, &mut stdout(), &mut stderr()) {
                Ok(_) => (),
                Err(x) => stdp::exit(x),
            }
        }

        println!("Doesn't do anything right now.");
    }
}
